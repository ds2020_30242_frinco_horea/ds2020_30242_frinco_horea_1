import React from "react";
import Table from "../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Birth date',
            accessor: 'birthDate',
        },
        {
            Header: 'Medical record',
            accessor: 'medicalRecord',
        },
        {
            Header: 'Patient username',
            accessor: 'userr.username',
            id: 'customId',
            show: false,
        },
        {
            Header: 'View',
            Cell: cell => (<button onClick={()=>props.handleClick(cell.original.userr.username, cell.original.name)}>View medication plans</button>)
        }
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={filters}
            pageSize={5}
        />
    )
}


const filters = [
    {
        placeholder: 'Search by name',
        accessor: 'name'
    }
];

class CaregiverPatientsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} handleClick={this.props.handleClick}/> )
    }
}

export default CaregiverPatientsTable;