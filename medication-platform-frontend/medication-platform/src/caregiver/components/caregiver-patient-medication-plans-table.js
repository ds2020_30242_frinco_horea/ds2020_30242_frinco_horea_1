import React from "react";
import Table from "../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: 'Patient name',
            accessor: 'patient.name',
        },
        {
            Header: 'Medication name',
            accessor: 'medication.name',
        },
        {
            Header: 'Dosage',
            accessor: 'medication.dosage',
        },
        {
            Header: 'Interval',
            accessor: 'interval',
        },
        {
            Header: 'Duration',
            accessor: 'duration',
        }
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={[]}
            pageSize={5}
        />
    )
}

class CaregiverPatientMedicationPlansTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
        console.log(this.state.tableData);
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} /> )
    }
}

export default CaregiverPatientMedicationPlansTable;