import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';
import * as API_CAREGIVERS from "./api/caregiver-api"
import Cookies from 'js-cookie'
import CaregiverPatientsTable from "./components/caregiver-patients-table";
import CaregiverPatientMedicationPlansTable from "./components/caregiver-patient-medication-plans-table";
import {Redirect} from "react-router-dom";
import HomeNavigation from "../home-navigation";

class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reloadMedicationPlansTable = this.reloadMedicationPlansTable.bind(this);
        this.fetchPatientMedicationPlan = this.fetchPatientMedicationPlan.bind(this);
        this.state = {
            username : Cookies.get('user'),
            selectedPatient: '',
            patientsTableData: [],
            patientsTableDataLoaded: false,
            medicationPlansTableData: [],
            medicationPlansTableDataLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchCorrespondingPatients();
    }

    fetchCorrespondingPatients() {
        return API_CAREGIVERS.fetchCorrespondingPatients(this.state.username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patientsTableData: result,
                    patientsTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatientMedicationPlan(patientUsername, patientName) {
        this.reloadMedicationPlansTable();
        return API_CAREGIVERS.fetchPatientMedicationPlan(patientUsername, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    medicationPlansTableData: result,
                    medicationPlansTableDataLoaded: true,
                    selectedPatient: patientName
                });
                console.log(this.state.medicationPlansTableData);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reloadMedicationPlansTable() {
        this.setState({
            medicationPlansTableData: [],
            medicationPlansTableDataLoaded: false,
            selectedPatient: ''
        });
        this.fetchCorrespondingPatients();
    }

    render() {

        if (!Cookies.get('role')) {
            return (
                <Redirect to = "/"/>
            )
        }
        else {
            let role = Cookies.get('role');
            if (role !== 'caregiver') {
                return (
                    <Redirect to = "/"/>
                )
            }
        }

        return (
            <div>
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Patients table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                                {this.state.patientsTableDataLoaded && <CaregiverPatientsTable
                                    tableData = {this.state.patientsTableData}
                                    handleClick={this.fetchPatientMedicationPlan}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            {this.state.selectedPatient !== '' && <strong> {this.state.selectedPatient}'s medication plans </strong>}
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>

                                {this.state.medicationPlansTableDataLoaded && <CaregiverPatientMedicationPlansTable tableData = {this.state.medicationPlansTableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />   }
                        </Card>
                    </Col>
                </Row>
            </div>
        )

    }
}

export default CaregiverContainer;
