import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver/patients',
    patientMedicationPlan: '/patient/medicationPlan'
};

function fetchCorrespondingPatients(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregiver + "?username=" + username, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function fetchPatientMedicationPlan(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.patientMedicationPlan + "?username=" + username, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    fetchCorrespondingPatients,
    fetchPatientMedicationPlan
};
