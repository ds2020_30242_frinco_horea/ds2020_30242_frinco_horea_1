import React from 'react';
import * as API_LOGIN from "./login-api";
import InputField from "../commons/toolbox/input-field";
import SubmitButton from "../commons/toolbox/submit-button";
import {Redirect} from 'react-router-dom'
import Cookies from 'js-cookie'

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            buttonDisabled: false,
            isLoggedIn: false
        }
    }

    setInputValue(property, val) {
        val = val.trim();
        if(val.length > 30) {
            return;
        }
        this.setState({
            [property]: val
        })
    }

    resetForm() {
        this.setState({
            username: '',
            password: '',
            buttonDisabled: false
        })
    }

    async doLogin() {

        if (!this.state.username) {
            return;
        }

        if (!this.state.password) {
            return;
        }

        this.setState({
            buttonDisabled: true
        })

        try {
            let credentials = {
                username: this.state.username,
                password: this.state.password
            };
            API_LOGIN.login(credentials, (result, status, error) => {
                if (status === 200) {
                    Cookies.set("isLoggedIn", true);
                    Cookies.set("user", result.username);
                    Cookies.set("role", result.role);
                    this.setState({
                        isLoggedIn: true
                    })
                }
                else {
                    this.resetForm();
                }
            });
        }
        catch (e) {
            console.log(e);
            this.resetForm();
        }
    }

    render() {
        if (this.state.isLoggedIn)
        {
            return (
                <Redirect to = "/"/>
            )
        }
        else
        {
            return (
                <div className="myContainer">
                    <div className="loginForm">
                        Log in
                        <InputField
                            type='text'
                            placeholder='Username'
                            value={this.state.username ? this.state.username : ''}
                            onChange={ (val) => this.setInputValue('username', val) }
                        />
                        <InputField
                            type='text'
                            placeholder='Password'
                            value={this.state.password ? this.state.password : ''}
                            onChange={ (val) => this.setInputValue('password', val) }
                        />
                        <SubmitButton
                            text='Login'
                            disabled={this.props.buttonDisabled}
                            onClick={ () => this.doLogin() }
                        />
                    </div>
                </div>
            )
        }
    }
}

export default LoginForm;