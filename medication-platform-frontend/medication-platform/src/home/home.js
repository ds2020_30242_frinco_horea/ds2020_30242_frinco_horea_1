import React from 'react';
import BackgroundImg from '../commons/images/home_bg.jpg';
import {Container, Jumbotron} from 'reactstrap';
import Cookies from "js-cookie"
import NavigationBar from "../navigation-bar";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'black', };

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.state = {
            isLoggedIn: false,
            username: '',
            role: 'default'
        }
    }

    logout() {
        Cookies.remove("isLoggedIn");
        Cookies.remove("user");
        Cookies.remove("role");
        Cookies.remove('patientSelected');
        Cookies.remove('patientId');
        this.resetForm();
    }

    resetForm() {
        this.setState({
            isLoggedIn: false,
            username: '',
            role: 'default'
        });
    }

    componentDidMount() {
        const isLoggedInCookie = Cookies.get("isLoggedIn");
        if (isLoggedInCookie) {
            this.setState({
                isLoggedIn: true,
                username: Cookies.get('user'),
                role: Cookies.get('role')
            })
        }
    }

    render() {
        return (
            <div>
                <NavigationBar logout={this.logout.bind(this)}/>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                    </Container>
                </Jumbotron>
            </div>
        )
    };
}

export default Home
