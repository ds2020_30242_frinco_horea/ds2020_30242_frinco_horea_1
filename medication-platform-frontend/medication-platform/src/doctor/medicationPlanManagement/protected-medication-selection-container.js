import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedMedicationSelectionContainer = ({ component: Component, role, patientSelected, ...rest }) => {
    return (
        <Route {...rest} render={
            props => {
                if (role === 'doctor' && patientSelected === 'true') {
                    return <Component {...rest} {...props} />
                } else if (role === 'doctor') {
                    return <Redirect to={
                        {
                            pathname: '/error400',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                } else {
                    return <Redirect to={
                        {
                            pathname: '/unauthorized',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        } />
    )
}

export default ProtectedMedicationSelectionContainer;