import React from 'react';
import * as API_DOCTORS from "../api/doctor-api";
import {Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import DoctorMedicationSelectionTable from "./components/doctor-medication-selection-table";
import Cookies from 'js-cookie'
import DoctorAddMedicationPlanForm from "./components/doctor-add-medication-plan-form";
import HomeNavigation from "../../home-navigation";

class MedicationSelectionContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.selectMedication = this.selectMedication.bind(this);
        this.state = {
            medicationTableData: [],
            medicationTableDataLoaded: false,
            selectedAddMedicationPlanForm: false,
            medicationPlanData: [],
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchAllMedication();
    }

    componentWillUnmount() {
        Cookies.remove('patientSelected');
        Cookies.remove('patientId');
    }

    fetchAllMedication() {
        let entityName = 'medication';
        return API_DOCTORS.fetchAll(entityName, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    medicationTableData: result,
                    medicationTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleAddMedicationPlanForm() {
        this.setState({
            selectedAddMedicationPlanForm: !this.state.selectedAddMedicationPlanForm
        });
    }

    reload() {
        this.setState({
            medicationTableData: [],
            medicationTableDataLoaded: false,
            selectedAddMedicationPlanForm: false
        });
        this.fetchAllMedication();
    }

    selectMedication(id) {
        let data = {
            medicationId: id,
            patientId: Cookies.get('patientId')
        }
        this.setState({
            selectedAddMedicationPlanForm: true,
            medicationPlanData: data,
        })
        console.log(data)
    }

    render() {
        console.log(this.state.medicationTableData);

        return (
            <div className="fullscreen">
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Medication table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                            {this.state.medicationTableDataLoaded && <DoctorMedicationSelectionTable
                                tableData = {this.state.medicationTableData}
                                handleClick={this.selectMedication}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Card>
                    </Col>
                </Row>

                <Modal isOpen={this.state.selectedAddMedicationPlanForm} toggle={this.toggleAddMedicationPlanForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMedicationPlanForm}> Add Medication Plan: </ModalHeader>
                    <ModalBody>
                        <DoctorAddMedicationPlanForm data={this.state.medicationPlanData} reloadHandler={this.reload} />
                    </ModalBody>
                </Modal>

            </div>
        )
    }
}

export default MedicationSelectionContainer;
