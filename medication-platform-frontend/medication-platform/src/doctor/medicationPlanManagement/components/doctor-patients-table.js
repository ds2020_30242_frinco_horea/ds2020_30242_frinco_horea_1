import React from "react";
import Table from "../../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: 'patientId',
            accessor: 'patientId',
            show: false
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Birth date',
            accessor: 'birthDate',
        },
        {
            Header: 'Medical record',
            accessor: 'medicalRecord',
        },
        {
            Header: 'Select',
            Cell: cell => (<button onClick={()=>props.handleClick(cell.original.patientId)}>Select patient</button>)
        }
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={filters}
            pageSize={5}
        />
    )
}


const filters = [
    {
        placeholder: 'Search by name',
        accessor: 'name'
    }
];

class DoctorPatientsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} handleClick={this.props.handleClick}/> )
    }
}

export default DoctorPatientsTable;