import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_DOCTORS from "../../api/doctor-api";
import APIResponseErrorMessage from "../../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';

class DoctorAddMedicationPlanForm extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;
        this.state = {
            errorStatus: 0,
            error: null,
            formIsValid: false,
            formControls: {
                interval: {
                    value: '',
                    placeholder: 'Interval',
                    valid: false,
                    touched: false,
                },
                duration: {
                    value: '',
                    placeholder: 'Duration',
                    valid: false,
                    touched: false,
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;
        const updatedControls = this.state.formControls;
        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedicationPlan(medicationPlan) {
        let entityName = 'medicationPlan';
        return API_DOCTORS.post(entityName, medicationPlan, (result, status, error) => {
            console.log(result)
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication plan with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medicationPlan = {
            interval: this.state.formControls.interval.value,
            duration: this.state.formControls.duration.value,
            medicationId: this.props.data.medicationId,
            patientId: this.props.data.patientId
        };

        console.log(medicationPlan);
        this.registerMedicationPlan(medicationPlan);
    }

    render() {
        return (
            <div>
                <FormGroup id='interval'>
                    <Label for='intervalField'> Interval: </Label>
                    <Input name='interval' id='intervalField' placeholder={this.state.formControls.interval.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.interval.value}
                           touched={this.state.formControls.interval.touched? 1 : 0}
                           valid={this.state.formControls.interval.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='duration'>
                    <Label for='durationField'> Duration: </Label>
                    <Input name='duration' id='durationField' placeholder={this.state.formControls.duration.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.duration.value}
                           touched={this.state.formControls.duration.touched? 1 : 0}
                           valid={this.state.formControls.duration.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        );
    }
}

export default DoctorAddMedicationPlanForm;
