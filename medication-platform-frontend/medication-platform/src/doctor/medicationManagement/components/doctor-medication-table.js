import React from "react";
import Table from "../../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: '',
            accessor: 'medicationId',
            show: false,
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Side effects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: 'Delete',
            Cell: cell => (<button onClick={()=>props.handleDelete(cell.original.name)}>Delete medication</button>)
        },
        {
            Header: 'Update',
            Cell: cell => (
                <button onClick={() => props.handleUpdate (
                    cell.original.medicationId,
                    cell.original.name,
                    cell.original.sideEffects,
                    cell.original.dosage)
                }>Update medication
                </button>)
        },
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={filters}
            pageSize={5}
        />
    )
}


const filters = [
    {
        placeholder: 'Search by name',
        accessor: 'name'
    }
];

class DoctorMedicationTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} handleDelete={this.props.handleDelete} handleUpdate={this.props.handleUpdate}/> )
    }
}

export default DoctorMedicationTable;