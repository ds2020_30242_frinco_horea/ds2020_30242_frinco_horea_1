import React from 'react';
import * as API_DOCTORS from "../api/doctor-api";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import DoctorPatientsTable from "./components/doctor-patients-table";
import DoctorAddPatientForm from "./components/doctor-add-patient-form";
import DoctorUpdatePatientForm from "./components/doctor-update-patient-form";
import HomeNavigation from "../../home-navigation";

class DoctorPatientsContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.deletePatient = this.deletePatient.bind(this);
        this.updatePatient = this.updatePatient.bind(this);
        this.toggleAddPatientForm = this.toggleAddPatientForm.bind((this));
        this.toggleUpdatePatientForm = this.toggleUpdatePatientForm.bind((this));
        this.state = {
            patientsTableData: [],
            patientsTableDataLoaded: false,
            selectedAddPatientForm: false,
            selectedUpdatePatientForm: false,
            updateData: [],
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchAllPatients();
    }

    fetchAllPatients() {
        let entityName = 'patient';
        return API_DOCTORS.fetchAll(entityName,(result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patientsTableData: result,
                    patientsTableDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    deletePatient(username) {
        let entityName = 'patient';
        return API_DOCTORS.deleteByUsername(entityName, username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.reload();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    updatePatient(patientId, caregiverId, name, gender, birthDate, address, medicalRecord, username, password, role) {
        let data = {
            patientId: patientId,
            caregiverId: caregiverId,
            name: name,
            gender: gender,
            birthDate: birthDate,
            address: address,
            medicalRecord: medicalRecord,
            username: username,
            password: password,
            role: role,
        }
        this.setState( {
            updateData: data
        })
        console.log(data);
        this.toggleUpdatePatientForm();
    }

    toggleAddPatientForm() {
        this.setState({
            selectedAddPatientForm: !this.state.selectedAddPatientForm
        });
    }

    toggleUpdatePatientForm() {
        this.setState({
            selectedUpdatePatientForm: !this.state.selectedUpdatePatientForm
        });
    }

    reload() {
        this.setState({
            patientsTableData: [],
            patientsTableDataLoaded: false,
            selectedAddPatientForm: false,
            selectedUpdatePatientForm: false
        });
        this.fetchAllPatients();
    }

    render() {
        console.log(this.state.patientsTableData);

        return (
            <div className="fullscreen">
                <HomeNavigation/>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <CardHeader>
                            <strong> Patients table </strong>
                        </CardHeader>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '12', offset: 0}}>
                        <Card>
                            {this.state.patientsTableDataLoaded && <DoctorPatientsTable
                                tableData = {this.state.patientsTableData}
                                handleDelete={this.deletePatient}
                                handleUpdate={this.updatePatient}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{size: '2', offset: 0}}>
                        <Button color="primary" onClick={this.toggleAddPatientForm}>Add Patient</Button>
                    </Col>
                </Row>

                <Modal isOpen={this.state.selectedAddPatientForm} toggle={this.toggleAddPatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddPatientForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <DoctorAddPatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdatePatientForm} toggle={this.toggleUpdatePatientForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdatePatientForm}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <DoctorUpdatePatientForm data={this.state.updateData} reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}

export default DoctorPatientsContainer;
