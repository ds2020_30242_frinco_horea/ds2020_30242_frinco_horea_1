import React from "react";
import Table from "../../../commons/tables/table";

const TheTable = props => {
    const columns = [
        {
            Header: '',
            accessor: 'patientId',
            show: false,
        },
        {
            Header: '',
            accessor: 'caregiver.caregiverId',
            show: false,
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Birth date',
            accessor: 'birthDate',
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Medical record',
            accessor: 'medicalRecord',
        },
        {
            Header: 'Patient username',
            accessor: 'userr.username',
        },
        {
            Header: 'Patient password',
            accessor: 'userr.password',
        },
        {
            Header: '',
            accessor: 'userr.role',
            show: false,
        },
        {
            Header: 'Delete',
            Cell: cell => (<button onClick={()=>props.handleDelete(cell.original.userr.username)}>Delete patient</button>)
        },
        {
            Header: 'Update',
            Cell: cell => (
                <button onClick={() => props.handleUpdate (
                    cell.original.patientId,
                    cell.original.caregiver.caregiverId,
                    cell.original.name,
                    cell.original.gender,
                    cell.original.birthDate,
                    cell.original.address,
                    cell.original.medicalRecord,
                    cell.original.userr.username,
                    cell.original.userr.password,
                    cell.original.userr.role)
                }>Update patient
                </button>)
        },
    ];

    return (
        <Table
            data={props.tableData}
            columns={columns}
            search={filters}
            pageSize={5}
        />
    )
}


const filters = [
    {
        placeholder: 'Search by name',
        accessor: 'name'
    }
];

class DoctorPatientsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return ( <TheTable tableData={this.state.tableData} handleDelete={this.props.handleDelete} handleUpdate={this.props.handleUpdate}/> )
    }
}

export default DoctorPatientsTable;