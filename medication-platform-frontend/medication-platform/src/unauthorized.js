import React from 'react';
import styles from './unauthorized.css'

const Unauthorized = () => {
    return (
        <div className="bg">
            <div id="sign-wrapper">
                <div id="hole1" className="hole"/>
                <div id="hole2" className="hole"/>
                <div id="hole3" className="hole"/>
                <div id="hole4" className="hole"/>
                <header id="header">
                    <h1>403 Forbidden</h1>
                </header>
                <section id="sign-body">
                    <div id="copy-container">
                        <h2>Authorized Personnel Only</h2>
                        <p><strong>Error 403: Forbidden</strong>. You do not have permission to view this page.</p>
                    </div>
                    <div id="circle-container">
                        <svg version="1.1" viewBox="0 0 500 500" preserveAspectRatio="xMinYMin meet">
                            <defs>
                                <pattern id="image" patternUnits="userSpaceOnUse" height="450" width="450">
                                    <image x="25" y="25" height="450" width="450" href="https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png"/>
                                </pattern>
                            </defs>
                            <circle cx="250" cy="250" r="200" strokeWidth="40px" stroke="#ef5350" fill="url(#image)"/>
                            <line x1="100" y1="100" x2="400" y2="400" strokeWidth="40px" stroke="#ef5350"/>
                        </svg>
                    </div>
                </section>
            </div>
        </div>
    )
}

export default Unauthorized;