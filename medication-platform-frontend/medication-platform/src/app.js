import React                                    from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home                                     from './home/home';
import LoginForm                                from './login/login-form';
import ErrorPage                                from './commons/errorhandling/error-page';
import styles                                   from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import Unauthorized from "./unauthorized";
import ProtectedPatientContainer from "./patient/protected-patient-container";
import ProtectedLoginForm from "./login/protected-login-form";
import ProtectedCaregiverContainer from "./caregiver/protected-caregiver-container";
import Cookies from 'js-cookie'
import Error400 from "./error-400";
import DoctorPatientsContainer from "./doctor/patientManagement/doctor-patients-container";
import ProtectedDoctorPatientsContainer from "./doctor/patientManagement/protected-doctor-patients-container";
import DoctorCaregiversContainer from "./doctor/caregiverManagement/doctor-caregivers-container";
import ProtectedDoctorCaregiversContainer from "./doctor/caregiverManagement/protected-doctor-caregivers-container";
import ProtectedDoctorMedicationContainer from "./doctor/medicationManagement/protected-doctor-medication-container";
import DoctorMedicationContainer from "./doctor/medicationManagement/doctor-medication-container";
import ProtectedPatientSelectionContainer
    from "./doctor/medicationPlanManagement/protected-patient-selection-container";
import PatientSelectionContainer from "./doctor/medicationPlanManagement/patient-selection-container";
import MedicationSelectionContainer from "./doctor/medicationPlanManagement/medication-selection-container";
import ProtectedMedicationSelectionContainer
    from "./doctor/medicationPlanManagement/protected-medication-selection-container";

class App extends React.Component {

    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <Switch>
                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/unauthorized'
                            render={() => <Unauthorized/>}
                        />

                        <Route
                            exact
                            path='/error400'
                            render={() => <Error400/>}
                        />

                        <ProtectedDoctorPatientsContainer
                            exact
                            path='/doctor/patients'
                            role = {Cookies.get('role')}
                            component={DoctorPatientsContainer}
                        />

                        <ProtectedDoctorCaregiversContainer
                            exact
                            path='/doctor/caregivers'
                            role = {Cookies.get('role')}
                            component={DoctorCaregiversContainer}
                        />

                        <ProtectedDoctorMedicationContainer
                            exact
                            path='/doctor/medication'
                            role = {Cookies.get('role')}
                            component={DoctorMedicationContainer}
                        />

                        <ProtectedPatientSelectionContainer
                            exact
                            path='/doctor/patientSelection'
                            role = {Cookies.get('role')}
                            component={PatientSelectionContainer}
                        />

                        <ProtectedMedicationSelectionContainer
                            exact
                            path='/doctor/medicationSelection'
                            role = {Cookies.get('role')}
                            patientSelected = {Cookies.get('patientSelected')}
                            component={MedicationSelectionContainer}
                        />

                        <ProtectedCaregiverContainer
                            exact
                            path='/caregiver'
                            role = {Cookies.get('role')}
                            component={CaregiverContainer}
                        />

                        <ProtectedPatientContainer
                            exact
                            path='/patient'
                            role = {Cookies.get('role')}
                            component={PatientContainer}
                        />

                        <ProtectedLoginForm
                            exact
                            path='/login'
                            isLoggedIn={Cookies.get('isLoggedIn')}
                            component={LoginForm}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
