import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient',
    patientMedicationPlan: '/patient/medicationPlan'
};

function getPatientMedicationPlans(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.patientMedicationPlan + "?username=" + username, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function fetchPatientData(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "?username=" + username, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getPatientMedicationPlans,
    fetchPatientData
};
