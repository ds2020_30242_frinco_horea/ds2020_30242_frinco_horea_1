import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Card,
    CardHeader,
    Col,
    Row,
    Badge
} from 'reactstrap';
import * as API_PATIENTS from "./api/patient-api"
import MedicationPlansTable from "./components/medication-plans-table";
import Cookies from 'js-cookie'
import HomeNavigation from "../home-navigation";

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.state = {
            username : Cookies.get('user'),
            patientData: [],
            patientDataLoaded: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedicationPlans();
        this.fetchPatientData();
    }

    fetchMedicationPlans() {
        return API_PATIENTS.getPatientMedicationPlans(this.state.username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatientData() {
        return API_PATIENTS.fetchPatientData(this.state.username, (result, status, err) => {
            if (result !== null && status === 200) {
                this.setState({
                    patientData: result,
                    patientDataLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    reload() {
        this.setState({
            patientDataLoaded: false,
            isLoaded: false
        });
        this.fetchMedicationPlans();
        this.fetchPatientData();
    }

    render() {
        return (
            <div className="fullscreen">
                <HomeNavigation/>
                <CardHeader>
                    {this.state.isLoaded && <strong> Welcome {this.state.patientData.name} </strong>}
                </CardHeader>
                {
                    this.state.patientDataLoaded &&
                    <div >
                        <div>
                            <Col sm={{size: '12', offset: 0}}><h3><Badge color="secondary">Your data</Badge></h3></Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Name: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.name}</Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Username: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.userr.username}</Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Gender: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.gender}</Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Birth Date: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.birthDate}</Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Address: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.address}</Col>
                                </Row>
                            </Col>
                            <Col>
                                <Row>
                                    <Col sm={{ size: 2, order: 1, offset: 0 }}>Medical record: </Col>
                                    <Col sm={{ size: 0, order: 2, offset: 0 }}>{this.state.patientData.medicalRecord}</Col>
                                </Row>
                            </Col>
                        </div>
                        <div className="paddingTop">
                        <Col><h3><Badge color="secondary">You can view your medication plans in the table below</Badge></h3></Col>
                            <Col sm={{size: '12', offset: 0}}>
                                <Card>
                                    {this.state.isLoaded && <MedicationPlansTable tableData = {this.state.tableData}/>}
                                    {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                        errorStatus={this.state.errorStatus}
                                        error={this.state.error}
                                    />   }
                                </Card>
                            </Col>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default PatientContainer;
