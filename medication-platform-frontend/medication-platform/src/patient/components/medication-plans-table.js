import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Medication',
        accessor: 'medication.name',
    },
    {
        Header: 'Side effects',
        accessor: 'medication.side_effects',
    },
    {
        Header: 'Dosage',
        accessor: 'medication.dosage',
    },
    {
        Header: 'Interval',
        accessor: 'interval',
    },
    {
        Header: 'Duration',
        accessor: 'duration',
    },
];

class MedicationPlansTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default MedicationPlansTable;