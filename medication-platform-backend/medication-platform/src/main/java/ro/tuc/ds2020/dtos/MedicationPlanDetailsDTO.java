package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDetailsDTO extends RepresentationModel<MedicationPlanDetailsDTO> {
    private UUID medicationPlanId;
    private UUID medicationId;
    private UUID patientId;
    private String name;
    private String sideEffects;
    private String dosage;
    private String interval;
    private String duration;

    public MedicationPlanDetailsDTO() {
    }

    public MedicationPlanDetailsDTO(UUID medicationPlanId, UUID medicationId, UUID patientId,  String name, String side_effects, String dosage, String interval, String duration) {
        this.medicationPlanId = medicationPlanId;
        this.medicationId = medicationId;
        this.patientId = patientId;
        this.name = name;
        this.sideEffects = side_effects;
        this.dosage = dosage;
        this.interval = interval;
        this.duration = duration;
    }

    public UUID getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(UUID medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide_effects() {
        return sideEffects;
    }

    public void setSide_effects(String side_effects) {
        this.sideEffects = side_effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDetailsDTO medicationPlanDetailsDTO = (MedicationPlanDetailsDTO) o;
        return medicationPlanId == medicationPlanDetailsDTO.medicationPlanId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(medicationPlanId, name, sideEffects, dosage, interval, duration);
    }
}
