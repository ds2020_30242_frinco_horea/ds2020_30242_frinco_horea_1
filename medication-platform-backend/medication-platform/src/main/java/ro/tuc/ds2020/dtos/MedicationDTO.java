package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

public class MedicationDTO extends RepresentationModel<MedicationDTO> {
    private UUID medicationId;
    @NotNull
    private String name;
    @NotNull
    private String sideEffects;
    @NotNull
    private String dosage;

    public MedicationDTO() {
    }

    public MedicationDTO(UUID medicationId, String name, String sideEffects, String dosage) {
        this.medicationId = medicationId;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public MedicationDTO(String name, String sideEffects, String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;
    }

    public UUID getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(UUID medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String side_effects) {
        this.sideEffects = side_effects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return medicationId == medicationDTO.medicationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sideEffects, dosage);
    }
}
