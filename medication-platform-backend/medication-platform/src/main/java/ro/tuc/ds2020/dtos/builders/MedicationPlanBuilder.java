package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.MedicationPlan;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getMedication().getMedicationId(), medicationPlan.getPatient().getPatientId(), medicationPlan.getInterval(), medicationPlan.getDuration());
    }

    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getInterval(), medicationPlanDTO.getDuration());
    }
}

