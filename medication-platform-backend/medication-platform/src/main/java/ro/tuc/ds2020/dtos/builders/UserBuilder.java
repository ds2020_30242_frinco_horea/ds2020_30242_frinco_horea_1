package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.entities.Userr;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(Userr userr) {
        return new UserDTO(userr.getUsername(), userr.getPassword(), userr.getRole());
    }

    public static Userr toEntity(UserDTO userDTO) {
        return new Userr(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
    }

    public static Userr toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Userr(patientDetailsDTO.getUsername(), patientDetailsDTO.getPassword(), patientDetailsDTO.getRole());
    }

    public static Userr toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Userr(caregiverDetailsDTO.getUsername(), caregiverDetailsDTO.getPassword(), "caregiver");
    }
}
