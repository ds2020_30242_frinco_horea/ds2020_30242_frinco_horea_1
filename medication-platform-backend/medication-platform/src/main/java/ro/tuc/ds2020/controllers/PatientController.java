package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.MedicationPlanService;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final MedicationPlanService medicationPlanService;
    private final PatientService patientService;

    @Autowired
    public PatientController(MedicationPlanService medicationPlanService, PatientService patientService) {
        this.medicationPlanService = medicationPlanService;
        this.patientService = patientService;
    }

    @GetMapping(value = "/medicationPlan")
    public ResponseEntity<List<MedicationPlan>> getPatientMedicationPlans(@RequestParam("username") String username) {
        try
        {
            List<MedicationPlan> medicationPlans = medicationPlanService.getPatientMedicationPlans(username);
            return new ResponseEntity<>(medicationPlans, HttpStatus.OK);
        }
        catch (ResourceNotFoundException ex)
        {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping()
    public ResponseEntity<Patient> getPatientByUsername(@RequestParam("username") String username) {
        try
        {
            Patient patient = patientService.getPatientByUsername(username);
            return new ResponseEntity<>(patient, HttpStatus.OK);
        }
        catch (ResourceNotFoundException ex)
        {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
