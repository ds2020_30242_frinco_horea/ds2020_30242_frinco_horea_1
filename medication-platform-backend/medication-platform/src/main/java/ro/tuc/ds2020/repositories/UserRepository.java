package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ro.tuc.ds2020.entities.Userr;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<Userr, UUID> {
    @Transactional
    @Modifying
    void deleteByUsername(String username);

    @Query(value = "SELECT u " +
            "FROM Userr u " +
            "WHERE u.username = :username ")
    Optional<Userr> findByUsername(@Param("username") String username);
}
